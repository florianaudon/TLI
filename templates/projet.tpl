	{include file="header.tpl"}

{if !isset($session_user)}
	<div class="warning">
		<p>Vous devez être connecté pour accèder aux fonctionnalitées de cette page</p>
		<p><a href="../connexion/" title="Se connecter">Se connecter</a></p>
	</div>
{else}

	{if isset($id_add_avancement)}
		{include file="add_comment.tpl"}
	{/if}

	{if isset($id_change_avancement)}
		{include file="change_comment.tpl"}
	{/if}
	<div>
		{if isset($erreur)}
			<div class="error">{$erreur}</div>
		{/if}

		{if isset($modif_result)}
			{if $modif_result}
				<div class="success">Modification du commentaire réussie</div>

			{else}
				<div class="error">Echec de la modification du commentaire</div>
			{/if}

		{/if}

		{if isset($add_result)}
			{if $add_result}
				<div class="success">Ajout du commentaire réussie</div>

			{else}
				<div class="error">Echec de l'ajout du commentaire</div>
			{/if}

		{/if}

		{if isset($result_add_avancement)}
			{if $result_add_avancement}
				<div class="success">Evénement ajouté</div>
			{else}
				<div class="error">Erreur l'événement n'a pas pu être ajouté</div>
			{/if}


		{/if}

		<h1>Suivis de projet</h1>

		{if isset($liste_avancement_projet)}
			{if count($liste_avancement_projet)!=0}


		<div class="warning_information">	<h1>Votre participation au projet</h1>
		<p>Vous êtes acteur sur les points :</p>
			<ul>
				{$i=0}
				{$cpt=0}
				{foreach $liste_avancement_projet as $data}

					{if stristr($data['acteurs'],$session_user)}
						{$cpt=$i++}
						<li>{$data['objet']}</li>
					{/if}
				{foreachelse}

				{/foreach}

				{if $cpt==0}
					Vous participez à aucun point
				{/if}
			</ul>

			<p>Autres points pour lesquels vous n'ête pas acteur:</p>
			<ul>
				{foreach $liste_avancement_projet as $data}

					{if !stristr($data['acteurs'],$session_user)}
						<li>{$data['objet']}:
							<ul> Acteur(s):
								{foreach unserialize($data['acteurs']) as $nom_acteur}

									<li>{$nom_acteur}</li>
								{/foreach}
							</ul>
						</li>

					{/if}

				{/foreach}
			</ul>

		</div>
		<div class="success">{count($liste_avancement_projet)} résultats trouvés</div>

		<table class="table_projet">
			<thead>
				<tr>
					<th>Acteur</th>
					<th>Objet</th>
					<th>Commentaire</th>
					<th>Action</th>

				</tr>
			</thead>
			<tbody>

				{if isset($liste_avancement_projet)}

					{foreach $liste_avancement_projet as $data}
						<tr>
							<td>
							{foreach unserialize($data['acteurs']) as $nom_acteur}
								{if $nom_acteur==""}
									<div class="error">	Aucun acteur n'a été trouvée</div>
								{else}
									<p class="acteur">{$nom_acteur}</p>

								{/if}

							{/foreach}
							</td>
							<td>

								<p>{$data['objet']}</p>
							</td>
							<td>
								<p>Modifié le: {$data['log']}:</p>
								<div class="commentaire"><p>{$data['commentaire']}</p></div>

							</td>

							<td class="center_text">
								
								{if $data['objet']=="Création de l'outil"}
									<p>Aucune actions de disponible pour cette référence</p>
								{else}
								<p>Projet terminé</p>
								
								{/if}
							</td>



						</tr>
				{/foreach}
				{/if}
			</tbody>
		</table>

		{else}
			<div class="error">	Aucun résultat n'a été trouvée</div>
		{/if}
		{/if}


	</div>

	<div id="add_projet">

		{if isset($session_user)}
		<h1>Ajout d'un point d'avancement sur le projet</h1>
		<form action="../projet/" method="POST">
			<fieldset>

				<legend>Formulaire d'avancement du projet</legend>
				<div>
					<label for="Acteur">Acteurs:</label>
					<select multiple="multiple" size="6" class="select_acteur"  name="acteur[]" required>
						<option value="Amar Kantas">Amar Kantas</option>
						<option value="Florian Audon">Florian Audon</option>
						<option value="Loic Covarel">Loic Covarel</option>
						<option value="Loic Delobel">Zack Delobel</option>
						<option value="Valentin Karakus">Valentin Karakus</option>

					</select>
				</div>
				<div>
					<label for="Objet">Objet:</label>
					<input type="text" name="objet" required>
				</div>
				Commentaire:
				<div>

					<textarea id="commentaire" name="commentaire" required></textarea>
				</div>


				<input type="submit" value="Ajouter">

			</fieldset>
		</form>
	</div>
	{/if}

{/if}
{include file="footer.tpl"}
