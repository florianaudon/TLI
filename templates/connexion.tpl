{include file="header.tpl"}

<div id="auth">

	{if !isset($session_user)}

	<form action="../index.php/" method="POST">
		<fieldset>
			{if isset($retour)}
			{$retour}
			{/if}

			{if isset($connexion_inform)}
			<div class="success">Inscription réussie, veuillez vous authentifier</div>
			{/if}

			{if isset($updateprofil_inform)}
			<div class="success">Votre mot de passe est à présent modifié, veuillez vous authentifier</div>
			{/if}

			<h1>Authentification:</h1>
			<div>
				<label for="Login">Votre identifiant:</label>
				<input id="Login" type="text" name="login_connexion" placeholder="Votre identifiant" accesskey="i" required/>
			</div>
			<div>
				<label for="password">Votre mot de passe:</label>
				<input id="password" type="password" name="password_connexion" placeholder="Votre mot de passe" accesskey="m" required>
			</div>
			<input type="submit" value="Se connecter">
			<div>
				<a href="index.php?page=inscription">Pas encore inscrit?</a>
			</div>
		</fieldset>
	</form>
	
	{/if}

	
		{include file="rss.tpl"}

</div>

{include file="footer.tpl"}
