<div>
	<h1>Ajout d'un commentaire</h1>
	<table class="table_projet">
		<thead>
			<tr>
				<th>Acteur</th>
				<th>Objet</th>
				<th>Commentaire</th>
				
			</tr>
		</thead>
		<tbody>
			{if isset($liste_avancement_projet)}
			{foreach $liste_avancement_projet as $data}
			{if $data['id_avancement']==$id_add_avancement}
			<tr>
				<td>
					{foreach unserialize($data['acteurs']) as $nom_acteur}
					{if $nom_acteur==""}
					<div class="error_listing">	Aucun acteur n'a été trouvée</div>
					{else}
					<p>{$nom_acteur}</p>

					{/if}

					{/foreach}
				</td>
				<td>
					<p>{$data['objet']}</p>
				</td>
				<td>
					<div class="commentaire"><p>{$data['commentaire']}</p></div>
				</td>
				


			</tr>

			{/if}
			
			{foreachelse}
			<tr>
				<td colspan="3"><div class="error_listing">	Aucun résultat n'a été trouvée</div></td>
			</tr>
			{/foreach}
			{/if}
		</tbody>
	</table>
</div>



<form action="index.php?page=projet" method="POST">
	<fieldset>

		<legend>Ajout d'un commentaire:</legend>

	</div>

	<div  class="edit_commentaire">
		{foreach $liste_avancement_projet as $commentaire}
		{if $commentaire['id_avancement']==$id_add_avancement}
		<input type="hidden" name="id_commentaire" value="{$id_add_avancement}">
		
		<textarea id="add_commentaire" name="add_commentaire" required></textarea>

		{/if}

		{/foreach}

	</div>


	<input type="submit" value="Ajouter">

</fieldset>
</form>


