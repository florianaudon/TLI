{include file="header.tpl"}

<div id="container_liste">

	<h1>Listes de toutes les pathologies</h1>
			<div class="success">{count($liste_meridien)} méridiens trouvés</div>
		<div class="table-container">
		<table>
				<thead>
					<tr>
						<th>Code du méridien</th>
						<th>nom du méridien</th>
						<th>element du méridien</th>
					</tr>
				</thead>
			<tbody>
				{if isset($liste_meridien)}
				{foreach $liste_meridien as $data}
				<tr>
					<td>{$data['code']}</td>
					<td>{$data['nom']}</td>
					<td>{$data['element']}</td>
				</tr>
				{foreachelse}
				<tr>
					<td colspan="3"><div class="error">Aucun méridien n'a été trouvé</div></td>
				</tr>
				{/foreach}
				{/if}
			</tbody>
		</table>
		</div>
	</div>
	
{include file="footer.tpl"}
