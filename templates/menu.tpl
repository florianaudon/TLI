			<nav>
				<ul id="menu" class="menu">
					<li><a href="../connexion/" accesskey="a">Accueil</a></li>			
					<li><a href="../symptome/" accesskey="z" title="Liste par symptôme">symptôme</a></li>
					<li><a href="../pathologie/" accesskey="e" title="Liste par pathologies">pathologies</a></li>
					<li><a href="../meridien/" accesskey="r" title="Liste par médidien">méridien</a></li>
					<li><a href="../webographie/" accesskey="w" title="Webographie">Webographie</a></li>
					{if isset($session_user)}
					<li><a href="../profil/" accesskey="p" title="Mon profil">Mon Profil</a></li>
					{/if}
					<li><a href="../projet/" accesskey="t" title="Avancement du projet">Avancement du projet</a></li>
					{if isset($session_user)}
					<li><a href="../deconnection/" accesskey="d" title="Se deconnecter">Se déconnecter</a></li>
					{else}
					<li><a href="../inscription/" accesskey="y" title="S'inscrire">Inscription</a></li>


					{/if}
					
					
				</ul>
			</nav>
