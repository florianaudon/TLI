{include file="header.tpl"}

<div id="container_liste">
{if isset($session_user)}
	<form action="#" method="POST">

		<fieldset>
			<legend>Rechercher:</legend>
			<div id="symptomeSearch">
				<label for="tags">Recherche d'une pathologie par mot-clef</label>
				<input id="symptome" type="text" name="symptome" placeholder="mot-clef" accesskey="s" required="required" />
			

				<input type="button" onclick="recherche();" value="Rerchercher" />
				
			
			
			</div>

		</fieldset>
	</form>

		<div id="title_recherche">
			<h2>Liste des pathologies trouvées à partir de la recherche:</h2>
		</div>
	
	<div aria-live="polite" aria-relevant="text" id="resultat_research">
	</div>
	{else}
	<div class="warning"><p>Connectez-vous pour accèder à toutes les fonctionnalitées de cette page</p>
	<p><a href="../connexion/" title="Se connecter">Se connecter</a></p>
	</div>
{/if}
	<h1>Listes de toutes les pathologies</h1>
	<div class="success">{count($liste_patho)} pathologie trouvées</div>

	<div class="table-container">
		<table>
			<thead>
				<tr>
					<th>Code du méridien</th>
					<th>type</th>
					<th>description de la pathologie</th>
				</tr>
			</thead>
			<tbody>
				{if isset($liste_patho)}
				{foreach $liste_patho as $data}
				<tr>
					<td>{$data['mer']}</td>
					<td>{$data['type']}</td>
					<td>{$data['desc']}</td>
				</tr>
				{foreachelse}
				<tr>
					<td colspan="3"><div class="error">	Aucune pathologie n'a été trouvée</div></td>
				</tr>
				{/foreach}
				{/if}
			</tbody>
		</table>
	</div>
</div>

{include file="footer.tpl"}
