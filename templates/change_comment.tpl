<div class="center_text">
<h1>Modification de commentaire</h1>
<table class="table_projet">
	<thead>
		<tr>
			<th>Acteur</th>
			<th>Objet</th>
			<th>Commentaire</th>
		
		</tr>
	</thead>
	<tbody>
		{if isset($liste_avancement_projet)}
		{foreach $liste_avancement_projet as $data}
		{if $data['id_avancement']==$id_change_avancement}
		<tr>
			<td>
				{foreach unserialize($data['acteurs']) as $nom_acteur}
				{if $nom_acteur==""}
				<div class="error_listing">	Aucun acteur n'a été trouvée</div>
				{else}
				<p>{$nom_acteur}</p>

				{/if}

				{/foreach}
			</td>
				<td>
				<p>{$data['objet']}</p>
			</td>
			<td>
			<div class="commentaire"><p>{$data['commentaire']}</p></div>
			</td>
		


		</tr>

		{/if}
		
		{foreachelse}
		<tr>
			<td colspan="3"><div class="error_listing">	Aucun résultat n'a été trouvée</div></td>
		</tr>
		{/foreach}
		{/if}
	</tbody>
</table>
</div>



	<form action="index.php?page=projet" method="POST">
		<fieldset>

			<legend>Modification du commentaire:</legend>

		</div>

		<div  style="margin:2%;">
		{foreach $liste_avancement_projet as $commentaire}
			{if $commentaire['id_avancement']==$id_change_avancement}
				<input type="hidden" name="id_commentaire" value="{$id_change_avancement}">
				
				<textarea id="change_commentaire" name="change_commentaire" required>	{$commentaire['commentaire']}</textarea>

			{/if}

				{/foreach}

		</div>


		<input type="submit" value="Modifier">

	</fieldset>
</form>


