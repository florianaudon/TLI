{include file="header.tpl"}

<form action="../inscription/" method="POST">
	<fieldset>
	

		{if isset($session_user)}
			<div class='success'>
				<p> {ucfirst($session_user)}  vous êtes actuellment connecté!</p> 
				<p>vous n'avez pas besoins de vous inscrire :)</p>
				<p><a href="#" title="Se déconnecter">Se déconnecter</a></p>
			</div>; 
		 {else}
		 	{if isset($retour)}
			{$retour}
			{/if}
			<h1>Inscription:</h1>
		<div>
			<label for="Login_inscription">Votre login:</label>
			<input id="Login_inscription" type="text" name="login_inscription" placeholder="Votre identifiant" accesskey="i" required/>
		</div>
		<div>
			<label for="password_inscription">Votre mot de passe:</label>
			<input id="password_inscription" type="password" name="password_inscription" placeholder="Votre mot de passe" accesskey="m" required>
		</div>

		<div>
			<label for="confirm_password">Confirmation de votre mot de passe:</label>
			<input id="confirm_password" type="password" name="confirm_password" placeholder="Confirmer votre mot de passe" accesskey="m" required>
		</div>

	
	

		<input type="submit" value="S'inscrire">

	</fieldset>
</form>
		{/if}
		
{include file="footer.tpl"}
