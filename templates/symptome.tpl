{include file="header.tpl"}

<div id="container_liste">
	<h1>Listes de toutes les pathologies</h1>
		<div class="success">{count($liste_symptome)} symptomes trouvés</div>
		<div class="table-container">
			<table>
					<thead>
						<tr>
							<th>Symptome n°</th>
							<th>description du symptome</th>
						</tr>
					</thead>
				<tbody>
					{if isset($liste_symptome)}
					{foreach $liste_symptome as $data}
					<tr>
						<td>{$data['idS']}</td>
						<td>{$data['desc']}</td>
					</tr>
					{foreachelse}
					<tr>
						<td colspan="3"><div class="error">	Aucun symptome n'a été trouvé</div></td>
					</tr>
					{/foreach}
					{/if}
				</tbody>
			</table>
		</div>
	</div>

{include file="footer.tpl"}
