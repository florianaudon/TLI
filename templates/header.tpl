<!DOCTYPE html>
<html lang="fr">
<head>
	<title>Fil Rouge</title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!--menu style-->
	<link href="../css/main.css" rel="stylesheet" type="text/css" />
	<!--formulaire css-->
	<link href="../css/formulaire.css" rel="stylesheet" type="text/css" />
	<!-- menu css-->
	<link rel="stylesheet" href="../css/menu.css" media="screen" >
	<!-- tableau css-->
	<link rel="stylesheet" href="../css/table.css" media="screen" >
	<!--flux rss css-->
	<link rel="stylesheet" href="../css/rss.css" media="screen" > 
	<!--webographie css-->
	<link rel="stylesheet" href="../css/webographie.css" media="screen" >
	<!-- recherche liste css-->
	<link href="../css/recherche.css" rel="stylesheet" type="text/css" /> 
	<!--css print-->
	<link rel="stylesheet" media="print" href="../css/print.css" type="text/css" />
	<!--ui jquery css-->
	<link rel="stylesheet" href="../js/lib/jquery-ui/jquery-ui.css">
	<link rel="stylesheet" href="../js/lib/editeur_texte/jquery.cleditor.css" media="screen" >

	<!--avancement de projet css-->
	<link rel="stylesheet" href="../js/lib/editeur_texte/jquery.cleditor.css" media="screen" >
	<link rel="stylesheet"  href="../css/avancement_projet.css" type="text/css" /> 
	<!--jquery import js-->
	<script src="../js/lib/jquery/jquery.min.js"></script>
	<script src="../js/lib/jquery-ui/jquery-ui.min.js"></script>
	<!--js import-->
	<script src="../autocomplete/autocomplete-0.3.0.js"></script> 

	
	<!-- Editeur de texte-->
	<script src="../js/lib/editeur_texte/jquery.cleditor.js"></script>
	<script src="../js/projet_editeur.js"></script> 

	<!-- recherche mot-clés-->
		<script src="../js/recherche.js"></script> 

</head>
	<body>

		<header>
			<div id="banner">
				<img src="../images/banner.jpg" alt="banniere image">
			</div>
			<div class="container">
			
				{include file="menu.tpl"}
			
			</div>
		</header>
