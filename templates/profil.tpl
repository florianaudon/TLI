{include file="header.tpl"}

<div id="profil">
{if isset($session_user)}
<h1>Mon profil: {$session_user}</h1>
{/if}
{if isset($success)}
<div class="success">{$success}</div>
{/if}
{if isset($erreur)}
<div class='error'>{$erreur}</div>
{/if}
{if isset($warning)}
<div class="warning">{{$warning}}</div>
{else}
<form action ="../profil/" method="post">
		<fieldset>
				{if isset($retour)}
					{$retour}
				{/if}

			<h1>Changement du mot de passe</h1>

			<div>
				<label for="Login">Mot de passe actuel :</label>
				<input id="Login" type="password" name="current_password" placeholder="Mot de passe actuel" accesskey="m" required/>
			</div>
			<div>
				<label for="password">Nouveau mot de passe :</label>
				<input id="password" type="password" name="new_password" placeholder="Nouveau mot de passe" accesskey="n" required>
			</div>
			<div>
				<label for="password">Confirmation nouveau mot de passe :</label>
				<input id="confim_password" type="password" name="new_password_confirmation" placeholder="Confirmation nouveau mot de passe" accesskey="c" required>
			</div>
			<input type="submit" value="Modifier mon mot de passe">
		</fieldset>
	</form>
</div>
{/if}
		
{include file="footer.tpl"}
