<?php

session_start();

//import des fichiers 
require_once("config/conf.php");
require_once(BASE_URL_SMARTY."Smarty.class.php");

$smarty=new Smarty();

//Tests des variables
if(isset($_GET['page']) and strlen($_GET['page'])<15){
	$page=$_GET['page'];

}else{
	$page="";

}

if(isset($_GET['arg']) and strlen($_GET['arg'])<15){
	$arg=$_GET['arg'];

}else{
	$arg="";

}

if(isset($_GET['arg2']) and strlen($_GET['arg2'])<15){
	$arg2=$_GET['arg2'];

}else{
	$arg2="";

}


//Routages
switch($page){
	case "inscription":
	include(BASE_URL_CONTROLLEURS."/inscription.contr.php");
	break;

	case "connexion":
	include(BASE_URL_CONTROLLEURS."/connexion.contr.php");
	break;
	
	case "profil":
	include(BASE_URL_CONTROLLEURS."/profil.contr.php");
	break;

	case "pathologie":

		include(BASE_URL_CONTROLLEURS."/pathologie.contr.php");
	break;

	case "symptome":
		include(BASE_URL_CONTROLLEURS."/symptome.contr.php");
	break;

	case "meridien":
		include(BASE_URL_CONTROLLEURS."/meridien.contr.php");
	break;

	case "projet":

		include(BASE_URL_CONTROLLEURS."/projet.contr.php");
	break;

	case "deconnection":
		include(BASE_URL_CONTROLLEURS."/deconnection.contr.php");
	break;

	case "researchPatho":
	include(BASE_URL_CONTROLLEURS."/recherche.contr.php");
	break;

	case "get_thisPatho":
	include(BASE_URL_CONTROLLEURS."/recherche.contr.php");
	break;

	case "webographie":
	include(BASE_URL_CONTROLLEURS."/webographie.contr.php");
	break;
	
	default:
		include(BASE_URL_CONTROLLEURS."/connexion.contr.php");
	}

	?>
