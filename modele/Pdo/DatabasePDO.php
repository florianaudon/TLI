<?php

/**
* Simplification et securisation des requêtes SQL
*/
class DataBasePDO{

	/**
	* @var string nom de la base de donnée
	*/
	private $nameDatabase;
	/**
	* @var string le login de l'utilisateur de la base de donnée
	*/
	private $user;
	/**
	* @var string le mot de passe de connexion
	*/
	private $password;
	/**
	* @var PDO Le connecteur de base de donnee
	*/
	private $db;

	/**
	* Constructeur de la classe. Initialise les variables et le connecteu de base de donnee
	* @param string $nameDatabase Le nom de la base de donnée
	* @param string $user  Le login pour la connexion a la base de donnee
	* @param string $password Mot de passe de l'utilisateur de la base de donnee
	*/
	function __construct($nameDatabase,$user,$password){

		$this->nameDatabase=$nameDatabase;
		$this->user=$user;
		$this->password=$password;

		try {//creation de l'objet PDO
		
		$this->db = new PDO('mysql:host=localhost;dbname='.$this->nameDatabase.'',$this->user,$this->password,array(PDO::ERRMODE_EXCEPTION=>PDO::ERRMODE_WARNING));
		$this->db->exec("SET NAMES 'utf8';");		
		
		}catch (PDOException $e){ //erreur de connexion à la basse
			//print "Erreur : ".$e->getMessage();
		
			return $e;
		}
		//$this->db = null; //on ferme la connexion
	}

	/*demande de ressources */
	/**
	* Fonction permettant d'effectuer des requetes de type SELECT
	*
	*/
	public function RequestSql($requete,$Param){

		$etat=array();//réponse au client format json

		

		try{
			//préparation de la requete:
			$prepare=$this->db->prepare($requete);
			//test si notre requete contient des paramètres (si le table $Param=null);
			if($Param!=NULL){
				//Execution de la requete
				$prepare->execute($Param);
				//on test le nombre de ligne, si >1 on fetchAll sinon fetch:

				$result=$prepare->fetchAll(PDO::FETCH_ASSOC);

				return $result;	


//				return $prepare->rowCount();

				
			}
			else{

				//Execution de la requete
					//préparation de la requete:
				$prepare=$this->db->prepare($requete);
				$prepare->execute(null);
				


				$result=$prepare->fetchAll(PDO::FETCH_ASSOC);

				return $result;
//				return $prepare->rowCount();


			}
		}catch(Exception $e){

			echo $e;
		}

		
	}



	/* modification dans la base de données */
	public function UpdateSql($requete,$Param){

		try{
			//préparation de la requete:
			$prepare=$this->db->prepare($requete);
	
		
				//Execution de la requete
			$e=$prepare->execute($Param);
				if($e){
					return $e;
				}
				else{
				
					return false;
				}
				//on test le nombre de ligne, si >1 on fetchAll sinon fetch:

				
		}catch(Exception $e){

			echo $e;
		}
	}

	/* insertion dans la base de données*/
	public function AddEntry($requete,$Param){
		
		$etat=array();//réponse au client format json

		try{

			$strSQL =$this->db->prepare($requete);
			//execution de la requête 
			//echo "requete ".$requete."</br>";

			if($strSQL->execute($Param)){
				$etat=true;
			}else{

				$etat=false;


			}
			
			

			return  $etat;

			
		}
		catch(Exception $e){

			$etat=array("Resultat"=>false);

			return json_encode($etat);
		}
		

	}






}


?>
