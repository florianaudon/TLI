
<?php
require_once("./config/conf.php");

/**
* Classe permettant de lire des flux RSS
*/
class RssReader{

	/**
	* @var string tag rss
	*/
	private $rss_tags;
	private $rss_item_tag;
	/**
	* @var string l'url
	*/
	private $url;

	function __construct($url=""){
		$this->url=$url;
		$this->rss_tags=array(  
		'title',  
		'link',  
		'description'
	); 
		$this->rss_item_tag="item";
	}

	public function getData(){
	  $doc = new DOMdocument();  
	  $doc->load($this->url);  
	  $rss_array = array();  
	  $items = array();  
	  foreach($doc-> getElementsByTagName($this->rss_item_tag) as $node) {  
	    foreach($this->rss_tags AS $key => $value) {  
	      $items[$value] = $node->getElementsByTagName($value)->item(0)->nodeValue;  
	    }  
	    array_push($rss_array, $items);  
	  }  
	  return $rss_array;  
	}
}

?>
