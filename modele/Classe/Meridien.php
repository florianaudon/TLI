
<?php



require_once("./config/conf.php");
require_once(BASE_URL_MODEL."/Pdo/DatabasePDO.php");
//en test:
//require_once("../../config/conf.php");
//require_once("../Pdo/DatabasePDO.php");

/**
* Classe gerant les meridiens
*/
class Meridien{

	/**
	* @var PDO Connecteur de base de donnees
	*/	
	private $connectDb;

	/**
	* Initialise le connecteur de base de donnees
	*/
	function __construct(){
		$this->connectDb = new DatabasePDO(NAME_BDD,USER_BDD,PASSWORD_BDD);
		

	}

	/**
	* Returne l'ensemble des meridien contenu dans la base de donnees
	* @return string les meridiens sous forme de tableau
	*/
	public function getMeridien(){

		$resultat=$this->connectDb->RequestSql("SELECT  *  from meridien ",$Param=null);

		return $resultat;



		
	}


	
}

?>
