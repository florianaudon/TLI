<?php

require_once("./config/conf.php");
require_once(BASE_URL_MODEL."/Pdo/DatabasePDO.php");
//en test:
//require_once("../../config/conf.php");
//require_once("../Pdo/DatabasePDO.php");

/**
* Classe permettant de gere les symptomes
*/
class Symptome{

	/**
	* @var PDO Le connecteur de base de donnees
	*/
	private $connectDb;

	/**
	* Le constructeur, il cree une nouvelle connexion a la base de donnee
	*/
	function __construct(){
		$this->connectDb = new DatabasePDO(NAME_BDD,USER_BDD,PASSWORD_BDD);
	}


	/**
	* Liste tous les symptomes present dans la base de donnees
	* @return array Retourne un tableau contenant tout les symptome de la bdd
	*/	
	public function getSymptome(){

		$resultat=$this->connectDb->RequestSql("SELECT  *  from symptome ",$Param=null);

		return $resultat;



		
	}


	
}

?>
