<?php


//Importation du fichier de config
require_once("./config/conf.php");
require_once(BASE_URL_MODEL."/Pdo/DatabasePDO.php");

/**
* Connexion et inscription dans utilisateur dans la base de donnees
*/
class Utilisateur{

	/**
	* @var string le login de l'utilisateur
	*/
	private $login_user;
	/**
	* @var string le mot de passe de l'utilisateur
	*/
	private $password_user;
	/**
	* @var string la confirmation du mot de passe l'utilisateur
	*/
	private $confirmePassword;
	/**
	* @var PDO Le connecteur de base de donnees
	*/
	private $connectDb;




	/**
	* Constructeur de la classe. Remplie les attributs de la classe et hash les mots de passes
	* @param string $login_user Login de l'utilisateur
	* @param string $password Mot de passe de l'utilisateur
	* @param string $confirmePassword Confirmation du mot de passe de l'utilisateur
	*/
	function __construct($login_user,$password,$confirmePassword=""){

		$this->login_user=$login_user;
		//hash en sha256 salter avec le login     
		$this->password_user=crypt($password,'$5$rounds=11111$'.$this->login_user);
		$this->confirmePassword=crypt($confirmePassword,'$5$rounds=11111$'.$this->login_user);
		$this->connectDb = new DatabasePDO(NAME_BDD,USER_BDD,PASSWORD_BDD);



	}
	
	/**
	* Methode permettant d'inscrire l'utilisateur dans la base de donnees
	* @return integer Retourne 0 si il y a un echec ou si 0 ligne ont été ajouté. Sinon retourne 1
	*/
	public function inscription(){
		$resultat_requete="";
		$resultat=0;

		//Vérification de la disponibilité du Login_user:

		$resultat_requete=$this->connectDb->RequestSql("SELECT login_user from user where login_user=:login_user",$Param=array("login_user"=>$this->login_user));

		//Le login est disponible, on insert l'utilisateur::
		//pour les test: 
		if(count($resultat_requete)!=0){
			
			//Le login éxiste déjà
			return 	$resultat;


			
		}
		else{


			$resultat_requete=$this->connectDb->AddEntry("INSERT INTO `user`(`login_user`, `password_user`) VALUES (:login_user,:password_user)",$Param=array("login_user"=>$this->login_user,"password_user"=>$this->password_user));

			if($resultat_requete){
				//inscription terminée
				$resultat=1;
				return $resultat;
			}
			else{
				//erreur liées à la requete sql
				echo "resultat de la requete: ".$resultat_requete;
				$resultat=2;
				return $resultat;
			}

			
		}
	}

	/**
	* Methode permettant d'authentifier un utilisateur dans la base de donnees
	* @return bool Retourne true si l'authentification a reussie sinon retourne false
	*/
	public function authentification(){
		$authentification=false;
		$resultat_requete="";
		//$this->auth_check=$this->connectDb->RequestSql("SELECT login_user from user where login_user=:login_user and password_user=:password_user",$Param=array("login_user"=>$this->login_user,"password_user"=>$this->password_user));
		$resultat_requete=$this->connectDb->RequestSql("SELECT login_user,password_user from user where login_user=:login_user ",$Param=array("login_user"=>$this->login_user));
		
		if(count($resultat_requete)!=0){
			//isset sinon erreur lorsuq'on test avec un login inconnu -> on a un array{0} count renvoie 1:
			if(isset($resultat_requete['0']['password_user'])){
				$pass=$resultat_requete['0']['password_user'];
				$authentification=hash_equals($this->password_user,$pass);
			}
			
			return $authentification;
		}else{
			return false;
		}
	}

	/**
	* Methode permettant de mettra a jour un mot de passe d'un utilisateur
	* @return integer Retourne 0 si il y a un echec ou si 0 ligne ont été mise à jour. Sinon retourne 1
	*/
	public function updatePassword(){
		$retour="";
		$resultat_requete="";
		$resultat_requete=$this->connectDb->UpdateSql("UPDATE user SET password_user=:password_user WHERE login_user=:login_user",$Param=array("login_user"=>$this->login_user,"password_user"=>$this->password_user));
		
		if($resultat_requete){
			$retour=$resultat_requete;
		}
		else{
			$retour=0;
		}

		return $retour;
	}

}

if(!function_exists('hash_equals')) {
	function hash_equals($str1, $str2) {
		if(strlen($str1) != strlen($str2)) {
			return false;
		} else {
			$res = $str1 ^ $str2;
			$ret = 0;
			for($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);
				return !$ret;
		}
	}
}

?>
