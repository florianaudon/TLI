
<?php



require_once("./config/conf.php");
require_once(BASE_URL_MODEL."/Pdo/DatabasePDO.php");
//en test:
//require_once("../../config/conf.php");
//require_once("../Pdo/DatabasePDO.php");

/*
* Classe gerant le projet
*/
class Projet{
	
	/**
	* @var string Contient la liste des points d'avancement du projet
	*/	
	private $array_data=array();
	
	/**
	* @var string Contient les commentaires
	*/	
	private $commentaire;
	
	/**
	* @var PDO Connecteur PDO
	*/	
	private $connectDb;
	
	/**
	* @var string Contient l'objet du post
	*/	
	private $objet;

	/*
	* Constructeur de la classe. Initialise les attributs
	* @param string $array_data contient le contenu du poste a ajouter si il y a
	* @param string $commentaire contient le commentaire du poste si il y a
	* @param string $objet contient l'objet du poste si il y a
	*/
	function __construct($array_data="",$commentaire="",$objet=""){
		$this->connectDb = new DatabasePDO(NAME_BDD,USER_BDD,PASSWORD_BDD);
		$this->array_data=$array_data;
		$this->commentaire=$commentaire;
		$this->objet=$objet;

	}


	public function addAvancementProjet(){

		
		if(count($this->array_data!=0)){

			if(isset($_SESSION['user'])){
				$acteur=ucfirst($_SESSION['user']);
				$log=date("d-m-y à  H:i")." par ".$acteur;

				$Resultat=$this->connectDb->AddEntry("INSERT INTO `avancement`(`acteurs`,`objet`,`commentaire`,`log`) VALUES (:acteurs,:objet,:commentaire,:log)",$Param=array("acteurs"=>serialize($this->array_data),"objet"=>$this->objet,"commentaire"=>$this->commentaire,"log"=>$log));				

				return $Resultat;

			}

		}

	}

	public function getAvancementProjet(){

		//Vérification de la disponibilité du Login_user:


		
		$resultat=$this->connectDb->RequestSql("SELECT  *  from avancement  ORDER BY id_avancement asc",$Param=null);

		return $resultat;
		
	}

	public function changeCommentaire(){


		$resultat=$this->connectDb->UpdateSql("UPDATE avancement SET commentaire = :commentaire, log=:log WHERE id_avancement = :id_avancement",$this->array_data);

		return $resultat;
	}

	public function addCommentaire(){

		$this->connectDb = new AvancementBDD_pdo("projet_tli","tli_user","jesuisunuserdeTLI");
		$resultat=$this->connectDb->UpdateSql("UPDATE avancement SET commentaire = CONCAT(commentaire,:commentaire), log=:log WHERE id_avancement = :id_avancement",$this->array_data);

		return $resultat;
	}



	
}

?>
