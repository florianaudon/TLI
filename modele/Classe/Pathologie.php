
<?php



require_once("./config/conf.php");
require_once(BASE_URL_MODEL."/Pdo/DatabasePDO.php");
//en test:
//require_once("../../config/conf.php");
//require_once("../Pdo/DatabasePDO.php");

/**
* Classe permettant de géré les pathologie
*/
class Pathologie{

	/**
	* @var PDO Le connecteur de base de donnees
	*/
	private $connectDb;
	private $keyword;


	/**
	* Constructeur de la classe. Cree une nouvelle connexion avec la base de donnees
	*/
	function __construct($keyword=""){
		$this->keyword=$keyword;
		$this->connectDb = new DatabasePDO(NAME_BDD,USER_BDD,PASSWORD_BDD);

	}

	/**
	* Retourne la liste des pathologie
	* @return Array Retourne un tableau des pathologies
	*/
	public function getPathologie(){

		//Vérification de la disponibilité du Login_user:


		
		$resultat=$this->connectDb->RequestSql("SELECT DISTINCT *  from patho ",$Param=null);

		return $resultat;



		
	}

	public function getAutoComplete(){
		$complete=$this->connectDb->RequestSql("SELECT  `name` as `label` FROM keywords WHERE `name` LIKE :keyword LIMIT 0, 10",$Param=array("keyword"=>"%".$this->keyword."%"));
		$i=0;
		/* foreach($complete as $donnees ) {
            // On ajoute les données dans un tableau

            $suggestions[$i]['id'] = $donnees['idS'];
            $suggestions[$i++]['label'] = $donnees['desc'];
            //print_r($suggestions);
        }*/
        echo json_encode($complete);
        // On renvoie le données au format JSON pour le plugin

    }

    public function getThisPatho(){

    	$complete=$this->connectDb->RequestSql("SELECT p.`desc` as `label` FROM symptome s INNER JOIN symptPatho sp ON s.idS=sp.idS INNER JOIN patho p ON sp.idP=p.idP WHERE s.`desc` LIKE :keyword",$Param=array("keyword"=>"%".$this->keyword."%"));
        			/* foreach($complete as $donnees ) {
            // On ajoute les données dans un tableau

            $suggestions[$i]['id'] = $donnees['idS'];
            $suggestions[$i++]['label'] = $donnees['desc'];
            //print_r($suggestions);
        }*/
        echo json_encode($complete);

    }


}

?>
