<?php
/* Smarty version 3.1.30, created on 2017-03-15 10:56:56
  from "C:\xampp\htdocs\www\TLI\templates\add_comment.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58c90fe863c5d3_68571980',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f4b0a0e981221c62b31d072d933db9b90958d356' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\add_comment.html',
      1 => 1489571585,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58c90fe863c5d3_68571980 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div style="text-align:center;">
<h1>Ajout d'un commentaire</h1>
<table class="table_projet">
	<thead>
		<tr>
			<th>Acteur</th>
			<th>Objet</th>
			<th>Commentaire</th>
		
		</tr>
	</thead>
	<tbody>
		<?php if (isset($_smarty_tpl->tpl_vars['liste_avancement_projet']->value)) {?>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_avancement_projet']->value, 'data');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
?>
		<?php if ($_smarty_tpl->tpl_vars['data']->value['id_avancement'] == $_smarty_tpl->tpl_vars['id_add_avancement']->value) {?>
		<tr>
			<td>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, unserialize($_smarty_tpl->tpl_vars['data']->value['acteurs']), 'nom_acteur');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['nom_acteur']->value) {
?>
				<?php if ($_smarty_tpl->tpl_vars['nom_acteur']->value == '') {?>
				<div class="error_listing">	Aucun acteur n'a été trouvée</div>
				<?php } else { ?>
				<p><?php echo $_smarty_tpl->tpl_vars['nom_acteur']->value;?>
</p>

				<?php }?>

				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			</td>
			<td>
				<p><?php echo $_smarty_tpl->tpl_vars['data']->value['objet'];?>
</p>
			</td>
			<td>
				<p><?php echo $_smarty_tpl->tpl_vars['data']->value['commentaire'];?>
</p>
			</td>
		


		</tr>

		<?php }?>
		
		<?php
}
} else {
?>

		<tr>
			<td colspan="3"><div class="error_listing">	Aucun résultat n'a été trouvée</div></td>
		</tr>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		<?php }?>
	</tbody>
</table>
</div>



	<form action="index.php?page=projet" method="POST">
		<fieldset>

			<legend>Ajout d'un commentaire:</legend>

		</div>

		<div  style="margin:2%;">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_avancement_projet']->value, 'commentaire');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['commentaire']->value) {
?>
			<?php if ($_smarty_tpl->tpl_vars['commentaire']->value['id_avancement'] == $_smarty_tpl->tpl_vars['id_add_avancement']->value) {?>
				<input type="hidden" name="id_commentaire" value="<?php echo $_smarty_tpl->tpl_vars['id_add_avancement']->value;?>
">
				
				<textarea id="add_commentaire" name="add_commentaire" required></textarea>

			<?php }?>

				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


		</div>


		<input type="submit" value="Ajouter">

	</fieldset>
</form>


<?php }
}
