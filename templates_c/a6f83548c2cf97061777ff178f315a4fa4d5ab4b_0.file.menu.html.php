<?php
/* Smarty version 3.1.30, created on 2017-03-07 16:32:16
  from "S:\xampp\htdocs\TLI\templates\menu.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58bed28036bc27_53651321',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a6f83548c2cf97061777ff178f315a4fa4d5ab4b' => 
    array (
      0 => 'S:\\xampp\\htdocs\\TLI\\templates\\menu.html',
      1 => 1488900723,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58bed28036bc27_53651321 (Smarty_Internal_Template $_smarty_tpl) {
?>
			<nav>
			<ul id="menu" class="menu">
				<li><a href="#">Accueil</a></li>
				<li><a href="#pathologie">Pathologie</a>
					<span id="pathologie"></span>
					<ul class="sous-menu">
						<li><a href="#">Recherches des pathologies</a>
							<ul>
								<li><a href="#" title="Recherche par symptôme">Par symptôme</a></li>
								<li><a href="#" title="Recherche par pathologies">Par pathologies</a></li>
								<li><a href="#" title="Recherche par méridien">Par méridien</a></li>
								<li><a href="#" title="Recherche par caractéristiques">Par caractéristiques</a></li>
							</ul>
						</li>
						<li><a href="#">Liste des pathologie</a>
							<ul>
								<li><a href="#" title="Liste par symptôme">Par symptôme</a></li>
								<li><a href="#" title="Liste par pathologies">Par pathologies</a></li>
								<li><a href="#" title="Liste par médidien">Par méridien</a></li>
								<li><a href="#" title="Liste par caractéristique">Par caractéristiques</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="#espace-membres">Espace membres</a>
					<span id="espace-membres"></span>
					<ul class="sous-menu">
						<li><a href="#">Mon compte</a>
							<ul>
								<li><a href="#" title="Mon profil">Mon Profil</a></li>
								<li><a href="#" title="Modification de mot de passe">Modifier mon mot de passe</a></li>
								<li><a href="#" title="Deconnexion">Se déconnecter</a></li>

							</ul>
						</li>

					</ul>
				</li>
				<li><a href="#" title="Avancement du projet">Avancement du projet</a></li>
				<li>
					<form>
						<input type="text" name="recherche" id="recherche" placeholder="Recherche">
					</form>
				</li>
			</ul>
		</nav>

<?php }
}
