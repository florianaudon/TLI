<?php
/* Smarty version 3.1.30, created on 2017-02-17 14:32:37
  from "C:\xampp\htdocs\www\TLI\templates\index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58a6fb755ee8d4_18750614',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6b53a7f5a522a5f5260d5e121de5e97087ac075b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\index.html',
      1 => 1487338326,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58a6fb755ee8d4_18750614 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>

<html lang="fr">
<head>
	<title>File Rouge</title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!--menu style-->
	<link href="css/main.css" rel="stylesheet" type="text/css" />
	<!--formulaire css-->
	<link href="css/formulaire.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="css/menu.css" media="screen" > 
</head>
<body>

	<header>
		<div id="banner">
			<img src="images/banner.jpg" alt="banniere image">
		</div>
		<div class="container">
			<nav>
				<li><a href="#">Accueil</a></li>
				<li><a href="#pathologie">Pathologie</a>
					<span id="pathologie"></span>
					<ul class="sous-menu">
						<li><a href="#">Recherches des pathologies</a>
							<ul>
								<li><a href="#" title="Recherche par symptôme">Par symptôme</a></li>
								<li><a href="#" title="Recherche par pathologies">Par pathologies</a></li>
								<li><a href="#" title="Recherche par méridien">Par méridien</a></li>
								<li><a href="#" title="Recherche par caractéristiques">Par caractéristiques</a></li>
							</ul>
						</li>
						<li><a href="#">Liste des pathologie</a>
							<ul>
								<li><a href="#" title="Liste par symptôme">Par symptôme</a></li>
								<li><a href="#" title="Liste par pathologies">Par pathologies</a></li>
								<li><a href="#" title="Liste par médidien">Par méridien</a></li>
								<li><a href="#" title="Liste par caractéristique">Par caractéristiques</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="#espace-membres">Espace membres</a>
					<span id="espace-membres"></span>
					<ul class="sous-menu">
						<li><a href="#">Mon compte</a>
							<ul>
								<li><a href="#" title="Mon profil">Mon Profil</a></li>
								<li><a href="#" title="Modification de mot de passe">Modifier mon mot de passe</a></li>
								<li><a href="#" title="Deconnexion">Se déconnecter</a></li>

							</ul>
						</li>

					</ul>
				</li>
				<li><a href="#" title="Avancement du projet">Avancement du projet</a></li>
				<li>
					<form>
						<input type="text" name="recherche" id="recherche" placeholder="Recherche">
					</form>
				</li>
			</ul>
		</nav>
	</div>
</header>
<div id="auth">
	<form>
		<fieldset>
			<legend>Authentification:</legend>
			<div>
				<label for="Login">Votre identifiant:</label>
				<input id="Login" type="text" name="login" placeholder="Votre identifiant" accesskey="i" required/>
			</div>
			<div>
				<label for="password">Votre mot de passe:</label>
				<input id="password" type="pawword" name="password" placeholder="Votre mot de passe" accesskey="m" required>
			</div>
			<input type="submit" value="Se connecter">

		</fieldset>
	</form>
</div>



</body>
</html><?php }
}
