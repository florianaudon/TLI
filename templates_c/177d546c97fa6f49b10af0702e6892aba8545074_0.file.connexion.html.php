<?php
/* Smarty version 3.1.30, created on 2017-03-17 14:00:23
  from "C:\xampp\htdocs\www\TLI\templates\connexion.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58cbdde7602b41_19882798',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '177d546c97fa6f49b10af0702e6892aba8545074' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\connexion.html',
      1 => 1489746510,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.html' => 1,
    'file:footer.html' => 1,
  ),
),false)) {
function content_58cbdde7602b41_19882798 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div id="auth">
	<form action="index.php?page=connexion" method="POST">
		<fieldset>
			<?php if (isset($_smarty_tpl->tpl_vars['retour']->value)) {?>
				<?php echo $_smarty_tpl->tpl_vars['retour']->value;?>

			<?php }?>

			<?php if (isset($_smarty_tpl->tpl_vars['session_user']->value)) {?>
			
		

				<p>
					Flux RSS / Web service

				</p>

			<?php } else { ?>
			<legend>Authentification:</legend>
			<div>
				<label for="Login">Votre identifiant:</label>
				<input id="Login" type="text" name="login_connexion" placeholder="Votre identifiant" accesskey="i" required/>
			</div>
			<div>
				<label for="password">Votre mot de passe:</label>
				<input id="password" type="password" name="password_connexion" placeholder="Votre mot de passe" accesskey="m" required>
			</div>
			<input type="submit" value="Se connecter">
			<div>
				<a href="index.php?page=inscription">Pas encore inscrit?</a>
			</div>
		</fieldset>
	</form>
		<?php }?>

</div>
<?php $_smarty_tpl->_subTemplateRender("file:footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
