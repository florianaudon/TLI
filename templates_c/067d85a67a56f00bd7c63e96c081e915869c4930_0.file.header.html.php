<?php
/* Smarty version 3.1.30, created on 2017-03-17 15:02:08
  from "C:\xampp\htdocs\www\TLI\templates\header.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58cbec60915172_73628518',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '067d85a67a56f00bd7c63e96c081e915869c4930' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\header.html',
      1 => 1489759323,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:menu.html' => 1,
  ),
),false)) {
function content_58cbec60915172_73628518 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<title>File Rouge</title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!--menu style-->
	<link href="css/main.css" rel="stylesheet" type="text/css" />
	<!--formulaire css-->
	<link href="css/formulaire.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="css/menu.css" media="screen" >

	<link rel="stylesheet" href="css/table.css" media="screen" > 

	<!-- Editeur de texte-->
	<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"><?php echo '</script'; ?>
>
	<link rel="stylesheet" href="Editeur/jquery.cleditor.css" media="screen" >
	<?php echo '<script'; ?>
 src="Editeur/jquery.cleditor.js"><?php echo '</script'; ?>
> 

</head>
<body>

	<header>
		<div id="banner">
			<img src="images/banner.jpg" alt="banniere image">
		</div>
		<div class="container">
		
			<?php $_smarty_tpl->_subTemplateRender("file:menu.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
		</div>
	</header>

<?php }
}
