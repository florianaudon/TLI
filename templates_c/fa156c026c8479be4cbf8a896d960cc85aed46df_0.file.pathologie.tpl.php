<?php
/* Smarty version 3.1.30, created on 2017-05-09 15:29:49
  from "C:\xampp\htdocs\www\TLI\templates\pathologie.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5911c44d923335_34673594',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa156c026c8479be4cbf8a896d960cc85aed46df' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\pathologie.tpl',
      1 => 1494336481,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5911c44d923335_34673594 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div id="container_liste">
<?php if (isset($_smarty_tpl->tpl_vars['session_user']->value)) {?>
	<form action="#" method="POST">

		<fieldset>
			<legend>Rechercher:</legend>
			<div id="symptomeSearch">
				<label for="tags">Recherche d'une pathologie par mot-clef</label>
				<input id="symptome" type="text" name="symptome" placeholder="mot-clef" accesskey="s" required="required" />
			

				<input type="button" onclick="recherche();" value="Rerchercher" />
				
			
			
			</div>

		</fieldset>
	</form>

		<div id="title_recherche">
			<h2>Liste des pathologies trouvées à partir de la recherche:</h2>
		</div>
	
	<div aria-live="polite" aria-relevant="text" id="resultat_research">
	</div>
	<?php } else { ?>
	<div class="warning"><p>Connectez-vous pour accèder à toutes les fonctionnalitées de cette page</p>
	<p><a href="../connexion/" title="Se connecter">Se connecter</a></p>
	</div>
<?php }?>
	<h1>Listes de toutes les pathologies</h1>
	<div class="success"><?php echo count($_smarty_tpl->tpl_vars['liste_patho']->value);?>
 pathologie trouvées</div>

	<div class="table-container">
		<table>
			<thead>
				<tr>
					<th>Code du méridien</th>
					<th>type</th>
					<th>description de la pathologie</th>
				</tr>
			</thead>
			<tbody>
				<?php if (isset($_smarty_tpl->tpl_vars['liste_patho']->value)) {?>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_patho']->value, 'data');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
?>
				<tr>
					<td><?php echo $_smarty_tpl->tpl_vars['data']->value['mer'];?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['data']->value['type'];?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['data']->value['desc'];?>
</td>
				</tr>
				<?php
}
} else {
?>

				<tr>
					<td colspan="3"><div class="error">	Aucune pathologie n'a été trouvée</div></td>
				</tr>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				<?php }?>
			</tbody>
		</table>
	</div>
</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
