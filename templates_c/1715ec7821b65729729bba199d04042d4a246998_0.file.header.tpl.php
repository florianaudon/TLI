<?php
/* Smarty version 3.1.30, created on 2017-05-09 14:38:58
  from "C:\xampp\htdocs\www\TLI\templates\header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5911b86209fcb3_18527685',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1715ec7821b65729729bba199d04042d4a246998' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\header.tpl',
      1 => 1494333533,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:menu.tpl' => 1,
  ),
),false)) {
function content_5911b86209fcb3_18527685 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<title>Fil Rouge</title>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!--menu style-->
	<link href="../css/main.css" rel="stylesheet" type="text/css" />
	<!--formulaire css-->
	<link href="../css/formulaire.css" rel="stylesheet" type="text/css" />
	<!-- menu css-->
	<link rel="stylesheet" href="../css/menu.css" media="screen" >
	<!-- tableau css-->
	<link rel="stylesheet" href="../css/table.css" media="screen" >
	<!--flux rss css-->
	<link rel="stylesheet" href="../css/rss.css" media="screen" > 
	<!--webographie css-->
	<link rel="stylesheet" href="../css/webographie.css" media="screen" >
	<!-- recherche liste css-->
	<link href="../css/recherche.css" rel="stylesheet" type="text/css" /> 
	<!--css print-->
	<link rel="stylesheet" media="print" href="../css/print.css" type="text/css" />
	<!--ui jquery css-->
	<link rel="stylesheet" href="../js/lib/jquery-ui/jquery-ui.css">
	<link rel="stylesheet" href="../js/lib/editeur_texte/jquery.cleditor.css" media="screen" >

	<!--avancement de projet css-->
	<link rel="stylesheet" href="../js/lib/editeur_texte/jquery.cleditor.css" media="screen" >
	<link rel="stylesheet"  href="../css/avancement_projet.css" type="text/css" /> 
	<!--jquery import js-->
	<?php echo '<script'; ?>
 src="../js/lib/jquery/jquery.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="../js/lib/jquery-ui/jquery-ui.min.js"><?php echo '</script'; ?>
>
	<!--js import-->
	<?php echo '<script'; ?>
 src="../autocomplete/autocomplete-0.3.0.js"><?php echo '</script'; ?>
> 

	
	<!-- Editeur de texte-->
	<?php echo '<script'; ?>
 src="../js/lib/editeur_texte/jquery.cleditor.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="../js/projet_editeur.js"><?php echo '</script'; ?>
> 

	<!-- recherche mot-clés-->
		<?php echo '<script'; ?>
 src="../js/recherche.js"><?php echo '</script'; ?>
> 

</head>
	<body>

		<header>
			<div id="banner">
				<img src="../images/banner.jpg" alt="banniere image">
			</div>
			<div class="container">
			
				<?php $_smarty_tpl->_subTemplateRender("file:menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			
			</div>
		</header>
<?php }
}
