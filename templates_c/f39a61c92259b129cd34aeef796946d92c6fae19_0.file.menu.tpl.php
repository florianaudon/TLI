<?php
/* Smarty version 3.1.30, created on 2017-05-09 15:08:49
  from "C:\xampp\htdocs\www\TLI\templates\menu.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5911bf61940df6_87519806',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f39a61c92259b129cd34aeef796946d92c6fae19' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\menu.tpl',
      1 => 1494335320,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5911bf61940df6_87519806 (Smarty_Internal_Template $_smarty_tpl) {
?>
			<nav>
				<ul id="menu" class="menu">
					<li><a href="../connexion/" accesskey="a">Accueil</a></li>			
					<li><a href="../symptome/" accesskey="z" title="Liste par symptôme">symptôme</a></li>
					<li><a href="../pathologie/" accesskey="e" title="Liste par pathologies">pathologies</a></li>
					<li><a href="../meridien/" accesskey="r" title="Liste par médidien">méridien</a></li>
					<li><a href="../webographie/" accesskey="w" title="Webographie">Webographie</a></li>
					<?php if (isset($_smarty_tpl->tpl_vars['session_user']->value)) {?>
					<li><a href="../profil/" accesskey="p" title="Mon profil">Mon Profil</a></li>
					<?php }?>
					<li><a href="../projet/" accesskey="t" title="Avancement du projet">Avancement du projet</a></li>
					<?php if (isset($_smarty_tpl->tpl_vars['session_user']->value)) {?>
					<li><a href="../deconnection/" accesskey="d" title="Se deconnecter">Se déconnecter</a></li>
					<?php } else { ?>
					<li><a href="../inscription/" accesskey="y" title="S'inscrire">Inscription</a></li>


					<?php }?>
					
					
				</ul>
			</nav>
<?php }
}
