<?php
/* Smarty version 3.1.30, created on 2017-05-09 13:39:27
  from "C:\xampp\htdocs\www\TLI\templates\symptome.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5911aa6fe44628_23612202',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '30532363088efa63b63113043ab10802ee3defa2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\symptome.tpl',
      1 => 1494254817,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5911aa6fe44628_23612202 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div id="container_liste">
	<h1>Listes de toutes les pathologies</h1>
		<div class="success"><?php echo count($_smarty_tpl->tpl_vars['liste_symptome']->value);?>
 symptomes trouvés</div>
		<div class="table-container">
			<table>
					<thead>
						<tr>
							<th>Symptome n°</th>
							<th>description du symptome</th>
						</tr>
					</thead>
				<tbody>
					<?php if (isset($_smarty_tpl->tpl_vars['liste_symptome']->value)) {?>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_symptome']->value, 'data');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
?>
					<tr>
						<td><?php echo $_smarty_tpl->tpl_vars['data']->value['idS'];?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['data']->value['desc'];?>
</td>
					</tr>
					<?php
}
} else {
?>

					<tr>
						<td colspan="3"><div class="error">	Aucun symptome n'a été trouvé</div></td>
					</tr>
					<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

					<?php }?>
				</tbody>
			</table>
		</div>
	</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
