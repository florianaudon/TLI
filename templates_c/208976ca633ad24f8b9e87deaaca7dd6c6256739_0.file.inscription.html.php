<?php
/* Smarty version 3.1.30, created on 2017-03-07 09:14:00
  from "S:\xampp\htdocs\TLI\templates\inscription.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58be6bc81dcbe5_58615390',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '208976ca633ad24f8b9e87deaaca7dd6c6256739' => 
    array (
      0 => 'S:\\xampp\\htdocs\\TLI\\templates\\inscription.html',
      1 => 1488871923,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.html' => 1,
    'file:footer.html' => 1,
  ),
),false)) {
function content_58be6bc81dcbe5_58615390 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<form action="index.php?page=inscription" method="POST">
	<fieldset>
		<?php if (isset($_smarty_tpl->tpl_vars['erreur_connexion']->value)) {?>
			$erreur_connexion
		<?php }?>
		<legend>Inscription:</legend>
		<div>
			<label for="Login_inscription">Votre login:</label>
			<input id="Login_inscription" type="text" name="login_inscription" placeholder="Votre identifiant" accesskey="i" required/>
		</div>
		<div>
			<label for="password_inscription">Votre mot de passe:</label>
			<input id="password_inscription" type="password" name="password_inscription" placeholder="Votre mot de passe" accesskey="m" required>
		</div>

		<div>
			<label for="confirm_password">Confirmation de votre mot de passe:</label>
			<input id="confirm_password" type="password" name="confirm_password" placeholder="Confirmer votre mot de passe" accesskey="m" required>
		</div>

	
	

		<input type="submit" value="S'inscrire">

	</fieldset>
</form>
<?php $_smarty_tpl->_subTemplateRender("file:footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
