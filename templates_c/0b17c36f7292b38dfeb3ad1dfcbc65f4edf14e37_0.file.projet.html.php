<?php
/* Smarty version 3.1.30, created on 2017-03-19 13:47:16
  from "C:\xampp\htdocs\www\TLI\templates\projet.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58ce7dd4276aa6_08951332',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0b17c36f7292b38dfeb3ad1dfcbc65f4edf14e37' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\projet.html',
      1 => 1489927620,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.html' => 1,
    'file:add_comment.html' => 1,
    'file:change_comment.html' => 1,
    'file:footer.html' => 1,
  ),
),false)) {
function content_58ce7dd4276aa6_08951332 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php echo '<script'; ?>
>
	$(document).ready(function () { $("#commentaire").cleditor(); 
		$("#change_commentaire").cleditor();
		$("#add_commentaire").cleditor(); });
	<?php echo '</script'; ?>
>
	<?php if (!isset($_smarty_tpl->tpl_vars['session_user']->value)) {?>
	<div class="warning">Vous devez être connecté pour accèder aux fonctionnalitées de cette page</div>
	<?php } else { ?>

	<?php if (isset($_smarty_tpl->tpl_vars['id_add_avancement']->value)) {?>
	<?php $_smarty_tpl->_subTemplateRender("file:add_comment.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php }?>

	<?php if (isset($_smarty_tpl->tpl_vars['id_change_avancement']->value)) {?>
	<?php $_smarty_tpl->_subTemplateRender("file:change_comment.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php }?>
	<div>
		<?php if (isset($_smarty_tpl->tpl_vars['modif_result']->value)) {?>
		<?php if ($_smarty_tpl->tpl_vars['modif_result']->value) {?>
		<div class="success">Modification du commentaire réussie</div>

		<?php } else { ?>
		<div class="error">Echec de la modification du commentaire</div>
		<?php }?>

		<?php }?>
		<?php if (isset($_smarty_tpl->tpl_vars['add_result']->value)) {?>
		<?php if ($_smarty_tpl->tpl_vars['add_result']->value) {?>
		<div class="success">Ajout du commentaire réussie</div>

		<?php } else { ?>
		<div class="error">Echec de l'ajout du commentaire</div>
		<?php }?>

		<?php }?>

		<?php if (isset($_smarty_tpl->tpl_vars['result_add_avancement']->value)) {?>
		<?php if ($_smarty_tpl->tpl_vars['result_add_avancement']->value) {?>
		<div class="success">Evénement ajouté</div>
		<?php } else { ?>
		<div class="error">Erreur l'événement n'a pas pu être ajouté</div>
		<?php }?>


		<?php }?>
		<h1>Suivis de projet</h1>
		<?php if (isset($_smarty_tpl->tpl_vars['liste_avancement_projet']->value)) {?>
		<?php if (count($_smarty_tpl->tpl_vars['liste_avancement_projet']->value) != 0) {?>
		<div class="success"><?php echo count($_smarty_tpl->tpl_vars['liste_avancement_projet']->value);?>
 résultats trouvés</div>
		<div class="warning_information">Vous êtes acteur sur les points :
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_avancement_projet']->value, 'data');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
?>

			<?php if (stristr($_smarty_tpl->tpl_vars['data']->value['acteurs'],$_smarty_tpl->tpl_vars['session_user']->value)) {?>
			 <ul>
			 	<li><?php echo $_smarty_tpl->tpl_vars['data']->value['objet'];?>
</li>
			 </ul>
			<?php }?>

			

		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


		</div>

		<table class="table_projet">
			<thead>
				<tr>
					<th>Acteur</th>
					<th>Objet</th>
					<th>Commentaire</th>
					<th>Action</th>

				</tr>
			</thead>
			<tbody>
				<?php if (isset($_smarty_tpl->tpl_vars['liste_avancement_projet']->value)) {?>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_avancement_projet']->value, 'data');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
?>
				<tr>
					<td>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, unserialize($_smarty_tpl->tpl_vars['data']->value['acteurs']), 'nom_acteur');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['nom_acteur']->value) {
?>
						<?php if ($_smarty_tpl->tpl_vars['nom_acteur']->value == '') {?>
						<div class="error">	Aucun acteur n'a été trouvée</div>
						<?php } else { ?>
						<p><?php echo $_smarty_tpl->tpl_vars['nom_acteur']->value;?>
</p>

						<?php }?>

						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

					</td>
					<td>
						<p><?php echo $_smarty_tpl->tpl_vars['data']->value['objet'];?>
</p>
					</td>
					<td>
						<p>Modifier le: <?php echo $_smarty_tpl->tpl_vars['data']->value['log'];?>
:</p>
						<div class="commentaire"><p><?php echo $_smarty_tpl->tpl_vars['data']->value['commentaire'];?>
</p></div>

					</td>

					<td style="text-align: center;">
						<?php if (!isset($_smarty_tpl->tpl_vars['session_user']->value)) {?>
						<p>Vous devez être connecté pour accèder à cette fonctionnalitée</p>
						<?php } else { ?>
						<?php if ($_smarty_tpl->tpl_vars['data']->value['objet'] == "Création de l'outil") {?>
							<p>Aucune actions de disponible pour cette référence</p>
						<?php } else { ?>
						<p><a href="index.php?page=projet&change=<?php echo $_smarty_tpl->tpl_vars['data']->value['id_avancement'];?>
" title="modifier le commentaire"><!--<img alt="change image" width="10%" src="templates/change.jpg">--> Modifier le commentaire</a></p>
						<p><a href="index.php?page=projet&add=<?php echo $_smarty_tpl->tpl_vars['data']->value['id_avancement'];?>
" title="ajouter un commentaire"><!--<img alt="change image" width="10%" src="templates/add.png">-->Ajouter un commentaire</a></p>
						<?php }?>

						<?php }?>
						
					</td>



				</tr>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				<?php }?>
			</tbody>
		</table>

		<?php } else { ?>
		<div class="error">	Aucun résultat n'a été trouvée</div>
		<?php }?>
		<?php }?>


	</div>

	<div id="add_projet">

		<?php if (isset($_smarty_tpl->tpl_vars['session_user']->value)) {?>
		<h1>Ajout d'un point d'avancement sur le projet</h1>
		<form action="index.php?page=projet" method="POST">
			<fieldset>

				<legend>Formulaire d'avancement du projet</legend>
				<div style="margin: 2%;">
					<label for="Acteur">Acteurs:</label>
					<select multiple="multiple" size="6" style="width: 100%;padding:2%;" name="acteur[]" required>
						<option value="Amar Kantas">Amar Kantas</option>
						<option value="Florian Audon">Florian Audon</option>
						<option value="Loic Covarel">Loic Covarel</option>
						<option value="Loic Delobel">Loic Delobel</option>
						<option value="Valentin Karakus">Valentin Karakus</option>

					</select>
				</div>
				<div style="margin: 2%;">
					<label for="Objet">Objet:</label>
					<input type="text" name="objet" required>
				</div>
				Commentaire:
				<div  style="margin:2%;">

					<textarea id="commentaire" name="commentaire" required></textarea>
				</div>


				<input type="submit" value="Ajouter">

			</fieldset>
		</form>
	</div>
	<?php }?>

	<?php }?>
	
	<?php $_smarty_tpl->_subTemplateRender("file:footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
