<?php
/* Smarty version 3.1.30, created on 2017-05-09 15:14:07
  from "C:\xampp\htdocs\www\TLI\templates\profil.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5911c09f22f378_65303470',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5e6f4716d23ba4f4dc8dc9a6f55fb32eb645dbf6' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\profil.tpl',
      1 => 1494335642,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5911c09f22f378_65303470 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div id="profil">
<?php if (isset($_smarty_tpl->tpl_vars['session_user']->value)) {?>
<h1>Mon profil: <?php echo $_smarty_tpl->tpl_vars['session_user']->value;?>
</h1>
<?php }
if (isset($_smarty_tpl->tpl_vars['success']->value)) {?>
<div class="success"><?php echo $_smarty_tpl->tpl_vars['success']->value;?>
</div>
<?php }
if (isset($_smarty_tpl->tpl_vars['erreur']->value)) {?>
<div class='error'><?php echo $_smarty_tpl->tpl_vars['erreur']->value;?>
</div>
<?php }
if (isset($_smarty_tpl->tpl_vars['warning']->value)) {?>
<div class="warning"><?php ob_start();
echo $_smarty_tpl->tpl_vars['warning']->value;
$_prefixVariable1=ob_get_clean();
echo $_prefixVariable1;?>
</div>
<?php } else { ?>
<form action ="../profil/" method="post">
		<fieldset>
				<?php if (isset($_smarty_tpl->tpl_vars['retour']->value)) {?>
					<?php echo $_smarty_tpl->tpl_vars['retour']->value;?>

				<?php }?>

			<h1>Changement du mot de passe</h1>

			<div>
				<label for="Login">Mot de passe actuel :</label>
				<input id="Login" type="password" name="current_password" placeholder="Mot de passe actuel" accesskey="m" required/>
			</div>
			<div>
				<label for="password">Nouveau mot de passe :</label>
				<input id="password" type="password" name="new_password" placeholder="Nouveau mot de passe" accesskey="n" required>
			</div>
			<div>
				<label for="password">Confirmation nouveau mot de passe :</label>
				<input id="confim_password" type="password" name="new_password_confirmation" placeholder="Confirmation nouveau mot de passe" accesskey="c" required>
			</div>
			<input type="submit" value="Modifier mon mot de passe">
		</fieldset>
	</form>
</div>
<?php }?>
		
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
