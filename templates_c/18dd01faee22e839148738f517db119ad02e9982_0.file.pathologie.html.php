<?php
/* Smarty version 3.1.30, created on 2017-03-17 10:23:38
  from "C:\xampp\htdocs\www\TLI\templates\pathologie.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58cbab1aa5a432_03508845',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '18dd01faee22e839148738f517db119ad02e9982' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\pathologie.html',
      1 => 1489584662,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.html' => 1,
    'file:footer.html' => 1,
  ),
),false)) {
function content_58cbab1aa5a432_03508845 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div id="container_liste">
<h1>Zone de recherche à faire.. (recherche par mot-clef)</h1></br>
<h2>Listes de toutes les pathologies</h2></br>
<h3>revoir le html de cette page -> style dans les balises + voir si on garde le scroll ou pas.. si oui
faudrait mettre le scroll seulement dans le tbody du table..</h3>
</br>
<h3>attention la recherche de pathologie par mots clé n'est active que pour les personne authentifiées!</h3>
	<div class="success"><?php echo count($_smarty_tpl->tpl_vars['liste_patho']->value);?>
 pathologie trouvées</div>

<table>
	<thead>
		<tr>
			<th>Code du méridien</th>
			<th>type</th>
			<th>description de la pathologie</th>
		</tr>
	</thead>
	<tbody>
		<?php if (isset($_smarty_tpl->tpl_vars['liste_patho']->value)) {?>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_patho']->value, 'data');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
?>
		<tr>
			<td><?php echo $_smarty_tpl->tpl_vars['data']->value['mer'];?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['data']->value['type'];?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['data']->value['desc'];?>
</td>
		</tr>
		<?php
}
} else {
?>

		<tr>
			<td colspan="3"><div class="error">	Aucune pathologie n'a été trouvée</div></td>
		</tr>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		<?php }?>
	</tbody>
</table>
</div>
<?php $_smarty_tpl->_subTemplateRender("file:footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
