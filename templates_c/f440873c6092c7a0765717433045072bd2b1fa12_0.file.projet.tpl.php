<?php
/* Smarty version 3.1.30, created on 2017-05-09 15:27:33
  from "C:\xampp\htdocs\www\TLI\templates\projet.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5911c3c589c168_63740563',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f440873c6092c7a0765717433045072bd2b1fa12' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\projet.tpl',
      1 => 1494336411,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:add_comment.tpl' => 1,
    'file:change_comment.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5911c3c589c168_63740563 (Smarty_Internal_Template $_smarty_tpl) {
?>
	<?php $_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php if (!isset($_smarty_tpl->tpl_vars['session_user']->value)) {?>
	<div class="warning">
		<p>Vous devez être connecté pour accèder aux fonctionnalitées de cette page</p>
		<p><a href="../connexion/" title="Se connecter">Se connecter</a></p>
	</div>
<?php } else { ?>

	<?php if (isset($_smarty_tpl->tpl_vars['id_add_avancement']->value)) {?>
		<?php $_smarty_tpl->_subTemplateRender("file:add_comment.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php }?>

	<?php if (isset($_smarty_tpl->tpl_vars['id_change_avancement']->value)) {?>
		<?php $_smarty_tpl->_subTemplateRender("file:change_comment.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<?php }?>
	<div>
		<?php if (isset($_smarty_tpl->tpl_vars['erreur']->value)) {?>
			<div class="error"><?php echo $_smarty_tpl->tpl_vars['erreur']->value;?>
</div>
		<?php }?>

		<?php if (isset($_smarty_tpl->tpl_vars['modif_result']->value)) {?>
			<?php if ($_smarty_tpl->tpl_vars['modif_result']->value) {?>
				<div class="success">Modification du commentaire réussie</div>

			<?php } else { ?>
				<div class="error">Echec de la modification du commentaire</div>
			<?php }?>

		<?php }?>

		<?php if (isset($_smarty_tpl->tpl_vars['add_result']->value)) {?>
			<?php if ($_smarty_tpl->tpl_vars['add_result']->value) {?>
				<div class="success">Ajout du commentaire réussie</div>

			<?php } else { ?>
				<div class="error">Echec de l'ajout du commentaire</div>
			<?php }?>

		<?php }?>

		<?php if (isset($_smarty_tpl->tpl_vars['result_add_avancement']->value)) {?>
			<?php if ($_smarty_tpl->tpl_vars['result_add_avancement']->value) {?>
				<div class="success">Evénement ajouté</div>
			<?php } else { ?>
				<div class="error">Erreur l'événement n'a pas pu être ajouté</div>
			<?php }?>


		<?php }?>

		<h1>Suivis de projet</h1>

		<?php if (isset($_smarty_tpl->tpl_vars['liste_avancement_projet']->value)) {?>
			<?php if (count($_smarty_tpl->tpl_vars['liste_avancement_projet']->value) != 0) {?>


		<div class="warning_information">	<h1>Votre participation au projet</h1>
		<p>Vous êtes acteur sur les points :</p>
			<ul>
				<?php $_smarty_tpl->_assignInScope('i', 0);
?>
				<?php $_smarty_tpl->_assignInScope('cpt', 0);
?>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_avancement_projet']->value, 'data');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
?>

					<?php if (stristr($_smarty_tpl->tpl_vars['data']->value['acteurs'],$_smarty_tpl->tpl_vars['session_user']->value)) {?>
						<?php $_smarty_tpl->_assignInScope('cpt', $_smarty_tpl->tpl_vars['i']->value++);
?>
						<li><?php echo $_smarty_tpl->tpl_vars['data']->value['objet'];?>
</li>
					<?php }?>
				<?php
}
} else {
?>


				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


				<?php if ($_smarty_tpl->tpl_vars['cpt']->value == 0) {?>
					Vous participez à aucun point
				<?php }?>
			</ul>

			<p>Autres points pour lesquels vous n'ête pas acteur:</p>
			<ul>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_avancement_projet']->value, 'data');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
?>

					<?php if (!stristr($_smarty_tpl->tpl_vars['data']->value['acteurs'],$_smarty_tpl->tpl_vars['session_user']->value)) {?>
						<li><?php echo $_smarty_tpl->tpl_vars['data']->value['objet'];?>
:
							<ul> Acteur(s):
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, unserialize($_smarty_tpl->tpl_vars['data']->value['acteurs']), 'nom_acteur');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['nom_acteur']->value) {
?>

									<li><?php echo $_smarty_tpl->tpl_vars['nom_acteur']->value;?>
</li>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							</ul>
						</li>

					<?php }?>

				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			</ul>

		</div>
		<div class="success"><?php echo count($_smarty_tpl->tpl_vars['liste_avancement_projet']->value);?>
 résultats trouvés</div>

		<table class="table_projet">
			<thead>
				<tr>
					<th>Acteur</th>
					<th>Objet</th>
					<th>Commentaire</th>
					<th>Action</th>

				</tr>
			</thead>
			<tbody>

				<?php if (isset($_smarty_tpl->tpl_vars['liste_avancement_projet']->value)) {?>

					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_avancement_projet']->value, 'data');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
?>
						<tr>
							<td>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, unserialize($_smarty_tpl->tpl_vars['data']->value['acteurs']), 'nom_acteur');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['nom_acteur']->value) {
?>
								<?php if ($_smarty_tpl->tpl_vars['nom_acteur']->value == '') {?>
									<div class="error">	Aucun acteur n'a été trouvée</div>
								<?php } else { ?>
									<p class="acteur"><?php echo $_smarty_tpl->tpl_vars['nom_acteur']->value;?>
</p>

								<?php }?>

							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

							</td>
							<td>

								<p><?php echo $_smarty_tpl->tpl_vars['data']->value['objet'];?>
</p>
							</td>
							<td>
								<p>Modifié le: <?php echo $_smarty_tpl->tpl_vars['data']->value['log'];?>
:</p>
								<div class="commentaire"><p><?php echo $_smarty_tpl->tpl_vars['data']->value['commentaire'];?>
</p></div>

							</td>

							<td class="center_text">
								
								<?php if ($_smarty_tpl->tpl_vars['data']->value['objet'] == "Création de l'outil") {?>
									<p>Aucune actions de disponible pour cette référence</p>
								<?php } else { ?>
								<p>Projet terminé</p>
								
								<?php }?>
							</td>



						</tr>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				<?php }?>
			</tbody>
		</table>

		<?php } else { ?>
			<div class="error">	Aucun résultat n'a été trouvée</div>
		<?php }?>
		<?php }?>


	</div>

	<div id="add_projet">

		<?php if (isset($_smarty_tpl->tpl_vars['session_user']->value)) {?>
		<h1>Ajout d'un point d'avancement sur le projet</h1>
		<form action="../projet/" method="POST">
			<fieldset>

				<legend>Formulaire d'avancement du projet</legend>
				<div>
					<label for="Acteur">Acteurs:</label>
					<select multiple="multiple" size="6" class="select_acteur"  name="acteur[]" required>
						<option value="Amar Kantas">Amar Kantas</option>
						<option value="Florian Audon">Florian Audon</option>
						<option value="Loic Covarel">Loic Covarel</option>
						<option value="Loic Delobel">Zack Delobel</option>
						<option value="Valentin Karakus">Valentin Karakus</option>

					</select>
				</div>
				<div>
					<label for="Objet">Objet:</label>
					<input type="text" name="objet" required>
				</div>
				Commentaire:
				<div>

					<textarea id="commentaire" name="commentaire" required></textarea>
				</div>


				<input type="submit" value="Ajouter">

			</fieldset>
		</form>
	</div>
	<?php }?>

<?php }
$_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
