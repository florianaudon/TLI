<?php
/* Smarty version 3.1.30, created on 2017-03-07 21:44:03
  from "C:\xampp\htdocs\www\TLI\templates\changemdp.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58bf1b93a41164_60712899',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c2bf798641d05dffa53998ae6314076122f5511a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\changemdp.html',
      1 => 1488879412,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.html' => 1,
    'file:footer.html' => 1,
  ),
),false)) {
function content_58bf1b93a41164_60712899 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div id="mdp">
	<form>
		<fieldset>
			<legend>Mot de passe :</legend>
			<div>
				<label for="Login">Mot de passe actuel :</label>
				<input id="Login" type="text" name="login" placeholder="Mot de passe actuel" accesskey="m" required/>
			</div>
			<div>
				<label for="password">Nouveau mot de passe :</label>
				<input id="password" type="password" name="password" placeholder="Nouveau mot de passe" accesskey="n" required>
			</div>
			<div>
				<label for="password">Confirmation nouveau mot de passe :</label>
				<input id="password" type="password" name="password" placeholder="Confirmation nouveau mot de passe" accesskey="c" required>
			</div>
			<input type="submit" value="Modifier mon mot de passe">
		</fieldset>
	</form>
</div>
		<?php $_smarty_tpl->_subTemplateRender("file:footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		
		</body>
</html>

<?php }
}
