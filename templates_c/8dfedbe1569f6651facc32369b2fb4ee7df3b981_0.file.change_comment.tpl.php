<?php
/* Smarty version 3.1.30, created on 2017-05-08 16:44:30
  from "C:\xampp\htdocs\www\TLI\templates\change_comment.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5910844ebb0358_57913688',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8dfedbe1569f6651facc32369b2fb4ee7df3b981' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\change_comment.tpl',
      1 => 1494239482,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5910844ebb0358_57913688 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="center_text">
<h1>Modification de commentaire</h1>
<table class="table_projet">
	<thead>
		<tr>
			<th>Acteur</th>
			<th>Objet</th>
			<th>Commentaire</th>
		
		</tr>
	</thead>
	<tbody>
		<?php if (isset($_smarty_tpl->tpl_vars['liste_avancement_projet']->value)) {?>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_avancement_projet']->value, 'data');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
?>
		<?php if ($_smarty_tpl->tpl_vars['data']->value['id_avancement'] == $_smarty_tpl->tpl_vars['id_change_avancement']->value) {?>
		<tr>
			<td>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, unserialize($_smarty_tpl->tpl_vars['data']->value['acteurs']), 'nom_acteur');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['nom_acteur']->value) {
?>
				<?php if ($_smarty_tpl->tpl_vars['nom_acteur']->value == '') {?>
				<div class="error_listing">	Aucun acteur n'a été trouvée</div>
				<?php } else { ?>
				<p><?php echo $_smarty_tpl->tpl_vars['nom_acteur']->value;?>
</p>

				<?php }?>

				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

			</td>
				<td>
				<p><?php echo $_smarty_tpl->tpl_vars['data']->value['objet'];?>
</p>
			</td>
			<td>
			<div class="commentaire"><p><?php echo $_smarty_tpl->tpl_vars['data']->value['commentaire'];?>
</p></div>
			</td>
		


		</tr>

		<?php }?>
		
		<?php
}
} else {
?>

		<tr>
			<td colspan="3"><div class="error_listing">	Aucun résultat n'a été trouvée</div></td>
		</tr>
		<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		<?php }?>
	</tbody>
</table>
</div>



	<form action="index.php?page=projet" method="POST">
		<fieldset>

			<legend>Modification du commentaire:</legend>

		</div>

		<div  style="margin:2%;">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_avancement_projet']->value, 'commentaire');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['commentaire']->value) {
?>
			<?php if ($_smarty_tpl->tpl_vars['commentaire']->value['id_avancement'] == $_smarty_tpl->tpl_vars['id_change_avancement']->value) {?>
				<input type="hidden" name="id_commentaire" value="<?php echo $_smarty_tpl->tpl_vars['id_change_avancement']->value;?>
">
				
				<textarea id="change_commentaire" name="change_commentaire" required>	<?php echo $_smarty_tpl->tpl_vars['commentaire']->value['commentaire'];?>
</textarea>

			<?php }?>

				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


		</div>


		<input type="submit" value="Modifier">

	</fieldset>
</form>


<?php }
}
