<?php
/* Smarty version 3.1.30, created on 2017-05-09 13:39:36
  from "C:\xampp\htdocs\www\TLI\templates\meridien.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5911aa7824d577_05279877',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '88901776830818d0b808a2e2cde6f70371f2e153' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\meridien.tpl',
      1 => 1494254806,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5911aa7824d577_05279877 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div id="container_liste">

	<h1>Listes de toutes les pathologies</h1>
			<div class="success"><?php echo count($_smarty_tpl->tpl_vars['liste_meridien']->value);?>
 méridiens trouvés</div>
		<div class="table-container">
		<table>
				<thead>
					<tr>
						<th>Code du méridien</th>
						<th>nom du méridien</th>
						<th>element du méridien</th>
					</tr>
				</thead>
			<tbody>
				<?php if (isset($_smarty_tpl->tpl_vars['liste_meridien']->value)) {?>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['liste_meridien']->value, 'data');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
?>
				<tr>
					<td><?php echo $_smarty_tpl->tpl_vars['data']->value['code'];?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['data']->value['nom'];?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['data']->value['element'];?>
</td>
				</tr>
				<?php
}
} else {
?>

				<tr>
					<td colspan="3"><div class="error">Aucun méridien n'a été trouvé</div></td>
				</tr>
				<?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				<?php }?>
			</tbody>
		</table>
		</div>
	</div>
	
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
