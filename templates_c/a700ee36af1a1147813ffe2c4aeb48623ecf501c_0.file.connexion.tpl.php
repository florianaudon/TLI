<?php
/* Smarty version 3.1.30, created on 2017-05-09 15:22:45
  from "C:\xampp\htdocs\www\TLI\templates\connexion.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5911c2a5b0c947_85723675',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a700ee36af1a1147813ffe2c4aeb48623ecf501c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\www\\TLI\\templates\\connexion.tpl',
      1 => 1494335628,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:rss.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5911c2a5b0c947_85723675 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div id="auth">

	<?php if (!isset($_smarty_tpl->tpl_vars['session_user']->value)) {?>

	<form action="../index.php/" method="POST">
		<fieldset>
			<?php if (isset($_smarty_tpl->tpl_vars['retour']->value)) {?>
			<?php echo $_smarty_tpl->tpl_vars['retour']->value;?>

			<?php }?>

			<?php if (isset($_smarty_tpl->tpl_vars['connexion_inform']->value)) {?>
			<div class="success">Inscription réussie, veuillez vous authentifier</div>
			<?php }?>

			<?php if (isset($_smarty_tpl->tpl_vars['updateprofil_inform']->value)) {?>
			<div class="success">Votre mot de passe est à présent modifié, veuillez vous authentifier</div>
			<?php }?>

			<h1>Authentification:</h1>
			<div>
				<label for="Login">Votre identifiant:</label>
				<input id="Login" type="text" name="login_connexion" placeholder="Votre identifiant" accesskey="i" required/>
			</div>
			<div>
				<label for="password">Votre mot de passe:</label>
				<input id="password" type="password" name="password_connexion" placeholder="Votre mot de passe" accesskey="m" required>
			</div>
			<input type="submit" value="Se connecter">
			<div>
				<a href="index.php?page=inscription">Pas encore inscrit?</a>
			</div>
		</fieldset>
	</form>
	
	<?php }?>

	
		<?php $_smarty_tpl->_subTemplateRender("file:rss.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


</div>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
