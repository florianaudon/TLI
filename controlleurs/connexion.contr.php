<?php
// Fichier controlleur gérant la connexion

require(BASE_URL_CONTROLLEURS."/User.contr.class.php");
require_once(BASE_URL_MODEL."/Classe/RssReader.php");

//https://medworm.com/rss/medicalfeeds/therapies/Acupuncture-News.xml
$rssReader=new RssReader("http://flux.20minutes.fr/feeds/rss-bordeaux.xml");
$rss_flux=$rssReader->getData();
// Si il est connecté
if(isset($_SESSION['user'])){
		$session_user=htmlentities($_SESSION['user']);
		$smarty->assign("session_user",$session_user);
		$smarty->assign("rss_flux", $rss_flux);
		
}
// Test si les champs existent
elseif(isset($_POST['login_connexion'],$_POST['password_connexion'])){


	// Test si les champs sont differents de null
   	if($_POST['login_connexion'] !== "" && $_POST['password_connexion'] !== ""){
		
		// Attribution des champs
		$login=$_POST['login_connexion'];
		$password=$_POST['password_connexion'];	
		$user=new User_contr($login,$password);

		// Connexion de l'utilisateur
		$retour=$user->connexion();
		
		//Envoie du message de retour au template
		$smarty->assign("retour",$retour);

		//save des données saisies:
		$smarty->assign("login_saisie", $_POST['login_connexion']);
		$smarty->assign("password_saisie", $_POST['password_connexion']);

	}else{
		// Si l'utilisateur n'a pas rempli tous les champs
		$smarty->assign("retour","Veuillez remplir tous les champs");
	}
}

if(isset($_GET['inscription'])){
	if(htmlspecialchars($_GET['inscription'])=="success"){
		$connexion_inform="";
		$smarty->assign("connexion_inform", $connexion_inform);
		
	}
}

if(isset($_GET['updateprofil'])){
	if(htmlspecialchars($_GET['updateprofil'])=="success"){
		$updateprofil_inform="";
		$smarty->assign("updateprofil_inform", $updateprofil_inform);

	}
}
// Si l'utilisateur arrive sur la page, on lui retourne simplement la page de connexion
$smarty->assign("rss_flux", $rss_flux);
$smarty->display(BASE_URL_TEMPLATES."/connexion.tpl");

?>
