<?php

require_once(BASE_URL_MODEL."/Classe/Utilisateur.php");
require_once(BASE_URL_CONTROLLEURS."/functionFiltrage.php");

/**
* Classe permettant de gerer l'inscription et la connexion
* d'un utilisateur avant de l'envoye au modele
*/
class User_contr {
	/**
	* @var string  Login de l'utilisateur
	*/
	private $login="";
	/**
	* @var string Mot de passe de l'utilisateur
	*/
	private $pass="";
	/**
	* @var string Mot de passe de confirmation de l'utilisateur
	*/
	private $pass_confirmation="";

	/**
	* Constructeur de la classe. Remplissage des differents attributs + protection xss
	* @param string $login Login de l'utilisateur
	* @param string $pass Mot de passe de l'utilisateur
	* @param string $pass_confirmation Mot de passe de confirmation de l'utilisateur (facultatif)
	*/
	function __construct($login,$pass,$pass_confirmation=""){
		
		//Protection XSS
		$this->login=htmlspecialchars($login);
		$this->pass=htmlspecialchars($pass);
		$this->pass_confirmation=htmlspecialchars($pass_confirmation);
	}	
	
	/**
	* Fonction permettant d'inscire un utilisateur
	* @return string Retourne un message d'erreur ou de succes
	*/
	public function inscription(){

		$retour="";
		//Check des identifiants
		$retour.=$this->checklogin();		
		if($this->pass === $this->pass_confirmation){
			$retour.=$this->checkPassword();
		}else{
			$retour.="<div class='error'>Les mots de passes ne sont pas identique</div>";
		}

		//Si pas d'erreurs alors on inscris l'utilisateur
		if($retour==""){

			$user=new Utilisateur($this->login,$this->pass);
			$inscription=$user->inscription();
			switch ($inscription) {
				case '0':
				$retour="<div class=\"error\">Ce login n'est pas disponible</div>";

				break;

				case '1':
				header("Location: ../connexion/");
				break;

				case '2':
				$retour="<div class=\"error\">Erreur: Impossible de valider l'inscription, vous pouvez signaler cette erreur <a href='mailto:amarkantas@hotmail.fr'>ici</a></div>";
				break;
			}
	




	}
	return $retour;

}

	/**
	* Verification du login : taille + caractere illegal
	* @return string Retourne un message d'erreur ou une chaine vide
	*/
	private function checkLogin(){
		
		$erreur="";

		if(!checkIllegal($this->login)){
			$erreur.="<div class='error'>Caractères illegaux detectés</div>\n";
		}

		if(strlen($this->login)>15){
			$erreur.="<div class='error'>Le login est trop long (>15 caractères)</div>\n";
		}

		if(strlen($this->login)>15){
			$erreur.="<div class='error'>Le login est trop long (>15 caractères)</div>\n";
		}

		return $erreur;
	}

	/**
	* Verification du mot de passe : taille + complexite + caractere illegal
	* @return string Retourne un message d'erreur ou une chaine vide
	*/
	public function checkPassword(){
		$erreur="";

		if(strlen($this->pass)>25){
			$erreur.= "<div class='error'>Mot de passe trop long (>25 caractères)</div>\n";
		}

		if(strlen($this->pass)<10){
			$erreur.= "<div class='error'>Mot de passe trop court (<10 caractères)</div>\n";
		}

		if( !preg_match("#[A-Z]+#", $this->pass) ) {
			$erreur.= "<div class='error'>Le mot de passe doit contenir au moins une majuscule</div>\n";
		}

		if( !preg_match("#[0-9]+#", $this->pass) ) {
			$erreur.= "<div class='error'>Le mot de passe doit contenir au moins un chiffre</div>\n";
		}

		if( !preg_match("#[a-z]+#", $this->pass) ) {
			$erreur.= "<div class='error'>Le mot de passe doit contenir au moins une lettre minuscule</div>\n";
		}

		return $erreur;
	}
	
	/**
	* Fonction permettant de connecter un utilisateur
	* @return string Retourne un message d'erreur ou de succes
	*/
	public function connexion(){

		$retour="";

		// Verification des caractères speciaux pour le login
		if(!checkIllegal($this->login)){
			$retour.= "<div class='error'>Caractères illegaux detectés</div>\n";
		}else{		

			$user=new Utilisateur($this->login,$this->pass);
			if($user->authentification()){
				$retour.="<div class='success'>Authentification reussie</div>\n";
				$_SESSION['user']=htmlentities($this->login);
				header("Location: ../connexion/"); // Une fois connecte, on est redirige vers l'index
			}else{
				$retour.="<div class='error'>Login ou mot de passe incorrect</div>\n";
			}
		}
		
		return $retour;
		
	}
}	

?>
