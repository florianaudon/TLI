<?php
// Gestion de l'avancement du projet

require_once(BASE_URL_MODEL."/Classe/Projet.php");
require(BASE_URL_CONTROLLEURS."/functionFiltrage.php");

// Si l'utilisateur est connecté
if(isset($_SESSION['user'])){
	$session_user=htmlentities($_SESSION['user']);
	$smarty->assign("session_user",$session_user);
	//Met la premier lettre en majuscule
	$acteur=ucfirst($session_user);
	$log=date("d-m-Y à  H:i")." par ".$acteur;
}
//ajout d'un commentaire:
if(isset($_POST['add_commentaire'],$_POST['id_commentaire'])){
	// Generation de la date du commentaire
	$log_commentaire=date("d-m-Y à H:i");
	// Convertion des retours a la ligne en br et echappement des caracteres speciaux
	$commentaire=nl2br(stripslashes($_POST['add_commentaire']));

	
		$new_comment="<pre>".ucfirst($_SESSION['user'])." a ajouté un commentaire le ".$log_commentaire." :</pre>".$commentaire;
		$array_data=array("id_avancement"=>$_POST['id_commentaire'],"commentaire"=>$new_comment,"log"=>$log);

		$instance_add_commentaire=new Projet($array_data);
		$add_result=$instance_add_commentaire->addCommentaire();
		if($add_result==true){

			$smarty->assign("add_result",$add_result);
		//$smarty->display(BASE_URL_TEMPLATES."/projet.tpl");
		}
		else{
			$smarty->assign("add_result",$add_result);
		//$smarty->display(BASE_URL_TEMPLATES."/projet.tpl");
		}

	
	
	
}
//modification d'un commentaire
if(isset($_POST['change_commentaire'],$_POST['id_commentaire'])){

		$change_commentaire=nl2br(stripslashes($_POST['change_commentaire']));
	
		$array_data=array("id_avancement"=>$_POST['id_commentaire'],"commentaire"=>$change_commentaire,"log"=>$log);

		$instance_change_commentaire=new Projet($array_data);
		$modif_result=$instance_change_commentaire->changeCommentaire();
		if($modif_result==true){

			$smarty->assign("modif_result",$modif_result);
			//$smarty->display(BASE_URL_TEMPLATES."/projet.tpl");
		}
		else{
			$smarty->assign("modif_result",$modif_result);
			//$smarty->display(BASE_URL_TEMPLATES."/projet.tpl");
		}
	

}

//ajout d'un avancement sur le projet:
if(isset($_POST['acteur'],$_POST['commentaire'],$_POST['objet'])){


	//Test si le tableau d'acteur n'est pas vide
	if(count($_POST['acteur'])!=0){

		$erreur="";
		$commentaire=nl2br(stripslashes($_POST['commentaire']));
		$objet=htmlspecialchars($_POST['objet']);

		if(checkIllegal($objet)){
			
				$add_avancement_projet=new Projet($_POST['acteur'],$commentaire,$objet);
				$result_add_avancement=$add_avancement_projet->addAvancementProjet();
				if($result_add_avancement){
					$smarty->assign("result_add_avancement",$result_add_avancement);
				}
				else{
					$smarty->assign("result_add_avancement",$result_add_avancement);
				}
		}
		else{
			$erreur.="Impossible d'ajouter un évènement: Caractères illegaux detectés dans l'objet";
			$smarty->assign("erreur",$erreur);
		}

	}

}

	//on récupère la liste d'avancement:
$avancement_projet=new Projet();
$liste_avancement_projet=$avancement_projet->getAvancementProjet();
	//si on clique sur modifier:
if(isset($_GET['change'])){

	$id_change_avancement=$_GET['change'];
	$smarty->assign("id_change_avancement",$id_change_avancement);
	$smarty->assign("liste_avancement_projet",$liste_avancement_projet);
	$smarty->display(BASE_URL_TEMPLATES."/projet.tpl");
}
else{
	if(isset($_GET['add'])){
		$id_add_avancement=$_GET['add'];
		$smarty->assign("id_add_avancement",$id_add_avancement);
		$smarty->assign("liste_avancement_projet",$liste_avancement_projet);

	}
	
	$smarty->assign("liste_avancement_projet",$liste_avancement_projet);
	$smarty->display(BASE_URL_TEMPLATES."/projet.tpl");
}








?>
