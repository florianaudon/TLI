<?php
/**
* Fonction pouvant être utile dans l'ensemble des controlleurs
*/

/** 
* Fonction de verification de la présence des caractères speciaux dans une chaine
* @param string $string Chaine a verifie
* @return bool False si la presence de caractere speciaux est detectes sinon true
*/
function checkIllegal($string){
	$autorise="true";
	$illegal = "\"#$%^&*()+=-[]';,./{}|:<>?~";
	if(strpbrk($string, $illegal) !== false){
		$autorise=false; // Caractere illegal detecte
	}
	
	return $autorise;
}
?>
