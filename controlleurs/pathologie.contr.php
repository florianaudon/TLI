<?php
//require(BASE_URL_CONTROLLEURS."/User.contr.class.php");
require_once(BASE_URL_MODEL."/Classe/Pathologie.php");

// Si il est connecté
if(isset($_SESSION['user'])){
		$session_user=htmlentities($_SESSION['user']);
		$smarty->assign("session_user",$session_user);
		
}

$pathologie=new Pathologie();
$liste_patho=$pathologie->getPathologie();
$smarty->assign("liste_patho",$liste_patho);
$smarty->display(BASE_URL_TEMPLATES."/pathologie.tpl");

?>
