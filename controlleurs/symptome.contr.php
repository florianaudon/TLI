<?php
//require(BASE_URL_CONTROLLEURS."/User.contr.class.php");
require_once(BASE_URL_MODEL."/Classe/Symptome.php");

// Si il est connecté
if(isset($_SESSION['user'])){
		$session_user=htmlentities($_SESSION['user']);
		$smarty->assign("session_user",$session_user);
		
}

$symptome=new Symptome();
$liste_symptome=$symptome->getSymptome();
$smarty->assign("liste_symptome",$liste_symptome);
$smarty->display(BASE_URL_TEMPLATES."/symptome.tpl");

?>
