<?php
// Fichier controlleur gérant l'inscription

require(BASE_URL_CONTROLLEURS."/User.contr.class.php");
require_once(BASE_URL_MODEL."/Classe/Utilisateur.php");


// S'il est connecté
if(isset($_SESSION['user'])){
	$session_user=htmlentities(ucfirst($_SESSION['user']));
	$smarty->assign("session_user",$session_user);

		//test sur la confirmation du formulaire:
		if(isset($_POST['current_password'],$_POST['new_password'],$_POST['new_password_confirmation'])){

			// Test si les champs sont differents de null
			if($_POST['current_password'] !== "" && $_POST['new_password'] !== "" && $_POST['new_password_confirmation'] !== ""){

				// Attribution des champs
				$current_password=htmlspecialchars($_POST['current_password']);
				$new_password=htmlspecialchars($_POST['new_password']);
				$new_password_confirm=htmlspecialchars($_POST['new_password_confirmation']);
				$erreur="";
				$success="";

		

				//on test la politique du mot de passe choisis par l'utilisateur:
				$check_new_password=new User_contr($session_user,$new_password);
				$check_new_password=$check_new_password->checkPassword();
				if($check_new_password==""){
					//on vérifie que les nouveaux mot de passe sont correct:
					if($new_password==$new_password_confirm){
					//on test si le mot de passe actul correspond à l'utilisateur qui souhaite le modifier
					$instance_authentification=new Utilisateur($_SESSION['user'],$current_password);
						if($instance_authentification->authentification()){
							$instance_updatePassword=new Utilisateur($_SESSION['user'],$new_password);
								if($instance_updatePassword->updatePassword()){
									if($current_password==$new_password){
										$erreur.="Le nouveau mot de passe doit être différent du mot de passe actuel.";
									}
									else{
										header('Location: ../connexion/');  
									}
									    
								}
								else{
									$erreur.="Erreur: impossible de modifier le mot de passe, veuillez réessayer ultérieurement.";
											
								}
						}
						else{
							$erreur.="Le mot de passe actuel est incorrect";
											
						}
					}
					else{
					$erreur.="Le nouveau mot de passe est différent de la confirmation du mot de passe";
					}
				}
				else{
					$erreur.=$check_new_password;
									
				}
			$smarty->assign("erreur",$erreur);				
			}
				
		}

}
	
else{
	$warning="Vous devez être connecté pour accéder à cette page";
	$smarty->assign("warning",$warning);
}
$smarty->display(BASE_URL_TEMPLATES."/profil.tpl");



	?>
