<?php
// Fichier controlleur gérant l'inscription

require(BASE_URL_CONTROLLEURS."/User.contr.class.php");

// S'il est connecté
if(isset($_SESSION['user'])){
		$session_user=htmlentities($_SESSION['user']);
		$smarty->assign("session_user",$session_user);
}

// Test si les champs existent
if(isset($_POST['login_inscription']) &&
	isset($_POST['password_inscription']) &&
	isset($_POST['confirm_password'])){

	// Test si les champs sont differents de null
	if($_POST['login_inscription'] !== "" &&
		$_POST['password_inscription'] !== "" &&
		$_POST['confirm_password'] !== ""){

		// Attribution des champs
		$login=$_POST['login_inscription'];
		$password=$_POST['password_inscription'];
		$password_confirm=$_POST['confirm_password'];

		// Appel du controlleur pour l'inscription
		$user=new User_contr($login,$password,$password_confirm);
		$retour=$user->inscription();

		// Test du code de retour
		$smarty->assign("retour",$retour);
		$smarty->display(BASE_URL_TEMPLATES."/inscription.tpl");
	
	}else{// Si les champs ne sont pas tous remplis
		$smarty->assign("retour","Veuillez remplir tous les champs");
		$smarty->display(BASE_URL_TEMPLATES."/inscription.tpl");
	}
}else{// SI l'utilisateur n'a pas encore envoyé le formulaire
	$smarty->display(BASE_URL_TEMPLATES."/inscription.tpl");
}

?>
