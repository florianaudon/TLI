<?php
// Seulement si l'utilisateur est connecté
if(isset($_SESSION['user']) && $_SESSION['user'] != "" ){
		
		session_destroy();
		if(isset($_GET['updateprofil'])){
			$inform=htmlspecialchars($_GET['updateprofil']);
			if($inform=="success"){
				
				header("Location:../connexion/");
			}
		}
		else{
					header("Location:../connexion/");
		}

		
}

else{
	header("Location:../connexion/");
}

?>
